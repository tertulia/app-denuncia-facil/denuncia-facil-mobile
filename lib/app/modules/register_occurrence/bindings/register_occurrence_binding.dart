import 'package:get/get.dart';

import 'package:denuncia_facil_mobile/app/modules/register_occurrence/controllers/register_occurrence_controller.dart';

class RegisterOccurrenceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegisterOccurrenceController>(
      () => RegisterOccurrenceController(),
    );
  }
}
