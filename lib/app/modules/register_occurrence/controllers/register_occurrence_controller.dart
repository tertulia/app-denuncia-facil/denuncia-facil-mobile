import 'dart:convert';
import 'dart:io';

import 'package:denuncia_facil_mobile/app/data/user_data.dart';
import 'package:denuncia_facil_mobile/app/models/ocurrence_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:url_launcher/url_launcher.dart';

class RegisterOccurrenceController extends GetxController {
  String url = 'https://api-denuncia-facil.herokuapp.com/api/occurrences/';

  final formKeyRegisterOc = GlobalKey<FormState>();
  final controllerNumberBoard = TextEditingController();
  final controllerTitle = TextEditingController();
  final controllerLocation = TextEditingController();
  final controllerObs = TextEditingController();

  DateTime dateTime = DateTime.now().obs();
  String type = "Selecione o tipo".obs();
  bool anon = false.obs();

  List<String> listTypes = [
    "19/07 Estacionamento incorreto",
    "102/12 Acidente"
  ];

  File image;
  final picker = ImagePicker();

  String validatorNumber(String textNumber) {
    if (textNumber.isEmpty) {
      return 'número da placa é obrigatório';
    } else if (textNumber.length < 7) {
      return 'placa inválida';
    }
    return null;
  }

  String validatorAll(String textValidator) {
    if (textValidator.isEmpty) {
      return 'campo obrigatoria';
    } else if (textValidator.length < 4) {
      return 'no minimo 4 caracteres';
    }
    return null;
  }

  selectPhoto() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    if (pickedFile != null) {
      image = File(pickedFile.path);
      update();
    } else {
      print('No image selected.');
    }
  }

  alterAn(bool val) {
    anon = val;
    print(token);

    update();
  }

  alterType(String typeSelected) {
    type = typeSelected;

    update();
  }

  alterDate(DateTime date) {
    dateTime = date;
    update();
    return true;
  }

  call() async {
    const url = "tel:98992037525";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<String> convertImageToBase() async {
    if (image == null) {
      return null;
    } else {
      List<int> imageBytes = image.readAsBytesSync();
      return base64Encode(imageBytes);
    }
  }

  Future<void> registerOc(customProgressDialog) async {
    String imageBaseString = await convertImageToBase();

    if (formKeyRegisterOc.currentState.validate()) {
      try {
        OcurrenceModel ocurrenceModel = OcurrenceModel(
          licensePlate: controllerNumberBoard.text,
          occurrenceType: type,
          occurrenceTitle: controllerTitle.text,
          anonymous: anon,
          image: imageBaseString,
          location: controllerLocation.text,
          observation: controllerObs.text,
        );
        var _body = json.encode(ocurrenceModel.toJson());
        var response = await http.post(
          url,
          body: _body,
          headers: {
            "Authorization": "Token $token",
            "Content-Type": "application/json"
          },
        );
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');
        customProgressDialog.dismiss();

        Get.back();
      } catch (e) {
        customProgressDialog.dismiss();

        print(e.toString());
      }
    }
    customProgressDialog.dismiss();
  }

  @override
  // ignore: must_call_super
  void onInit() {}

  @override
  // ignore: must_call_super
  void onReady() {}

  @override
  void onClose() {}
}
