import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:denuncia_facil_mobile/app/shared/buttons.dart';
import 'package:denuncia_facil_mobile/app/shared/colors.dart';
import 'package:denuncia_facil_mobile/app/shared/inputs.dart';
import 'package:denuncia_facil_mobile/app/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:denuncia_facil_mobile/app/modules/register_occurrence/controllers/register_occurrence_controller.dart';

class RegisterOccurrenceView extends GetView<RegisterOccurrenceController> {
  @override
  Widget build(BuildContext context) {
    ArsProgressDialog customProgressDialog = ArsProgressDialog(context,
        blur: 2,
        loadingWidget: Container(
          width: 50,
          height: 50,
          child: CircularProgressIndicator(),
        ));
    return Scaffold(
      appBar: appBar(
        title: "Registrar ocorrencia",
      ),
      body: Container(
        margin: EdgeInsets.all(15),
        child: Form(
          key: controller.formKeyRegisterOc,
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    padding: const EdgeInsets.all(5.0),
                    child: raisedButton(
                        onPress: controller.call, text: "Ligar para SMTT"),
                  ),
                ],
              ),
              InkWell(
                onTap: controller.selectPhoto,
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  child: controller.image == null
                      ? Icon(
                          Icons.camera_alt,
                          size: 80,
                        )
                      : Image.file(
                          controller.image,
                          height: 120,
                        ),
                ),
              ),
              SizedBox(
                height: controller.image == null ? 80 : 10,
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10.0),
                child: textFormField(
                    labText: "Numero da placa",
                    cont: controller.controllerNumberBoard,
                    validator: controller.validatorNumber),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10.0),
                child: textFormField(
                  labText: "Titulo de ocorrencia",
                  cont: controller.controllerTitle,
                  validator: controller.validatorAll,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10.0),
                child: GetBuilder(
                  init: controller,
                  builder: (controller) => outLineButton(
                    text: controller.type,
                    onPress: () {
                      Get.dialog(
                        Center(
                          child: Card(
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.5,
                              width: MediaQuery.of(context).size.width * 0.7,
                              padding: EdgeInsets.all(30),
                              child: Column(
                                children: [
                                  Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.07,
                                      child: Text(controller.type)),
                                  Divider(),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.3,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: controller.listTypes.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        print(index);
                                        return ListTile(
                                            title: Text(
                                              controller.listTypes[index],
                                            ),
                                            onTap: () {
                                              controller.alterType(
                                                controller.listTypes[index],
                                              );
                                              Get.back();
                                            });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    icon: Icon(
                      Icons.arrow_drop_down,
                      color: primary,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10.0),
                child: textFormField(
                    labText: "Localizacao",
                    cont: controller.controllerLocation,
                    icon: Icon(
                      Icons.location_on,
                      color: primary,
                    )),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text("Denuncia anonima"),
                  GetBuilder(
                    init: controller,
                    builder: (controller) => Switch(
                      value: controller.anon,
                      onChanged: controller.alterAn,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10.0),
                child: textFormField(
                  labText: "Observacao",
                  lines: 3,
                  cont: controller.controllerObs,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: raisedButton(
                    onPress: () {
                      customProgressDialog.show();
                      controller.registerOc(customProgressDialog);
                    },
                    text: "Registrar"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
