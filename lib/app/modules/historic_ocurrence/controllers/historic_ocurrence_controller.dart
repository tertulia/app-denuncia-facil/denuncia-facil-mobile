import 'dart:convert';

import 'package:denuncia_facil_mobile/app/data/user_data.dart';
import 'package:denuncia_facil_mobile/app/models/ocurrence_model.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class HistoricOcurrenceController extends GetxController {
  String url = 'https://api-denuncia-facil.herokuapp.com/api/occurrences/';

  Future<List<OcurrenceModel>> getOcurrences() async {
    var response = await http.get(
      url,
      headers: {
        'Authorization': 'Token $token',
      },
    );
    List<OcurrenceModel> listOcurrence = List<OcurrenceModel>();

    List list = json.decode(response.body);
    list.forEach(
        (element) => listOcurrence.add(OcurrenceModel.fromJson(element)));
    print('Response status: ${response.headers}');
    print('Response body: ${response.body}');

    return listOcurrence;
  }

  @override
  void onClose() {}
}
