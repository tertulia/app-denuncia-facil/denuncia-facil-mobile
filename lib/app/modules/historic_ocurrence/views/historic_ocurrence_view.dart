import 'package:denuncia_facil_mobile/app/drawer/drawer.dart';
import 'package:denuncia_facil_mobile/app/models/ocurrence_model.dart';
import 'package:denuncia_facil_mobile/app/routes/app_pages.dart';
import 'package:denuncia_facil_mobile/app/shared/styles.dart';
import 'package:denuncia_facil_mobile/app/shared/widgets/app_bar.dart';
import 'package:denuncia_facil_mobile/app/shared/widgets/card_historic.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:denuncia_facil_mobile/app/modules/historic_ocurrence/controllers/historic_ocurrence_controller.dart';

class HistoricOcurrenceView extends GetView<HistoricOcurrenceController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        title: "Histórico de ocorrência",
      ),
      drawer: drawerWidget(context),
      body: FutureBuilder<List<OcurrenceModel>>(
          future: controller.getOcurrences(),
          builder: (context, snapshot) {
            if (snapshot.data != null) {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (_, index) {
                  return CardHistoricWidget(ocurrence: snapshot.data[index]);
                },
              );
            }
            return Center(
              child: Text(
                "Buscando ocorrencias...",
                style: textGrey18,
              ),
            );
          }),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Get.toNamed(Routes.REGISTER_OCCURRENCE);
        },
        label: Text(
          "Registrar ocorrência",
          style: titleBar,
        ),
        isExtended: true,
      ),
    );
  }
}
