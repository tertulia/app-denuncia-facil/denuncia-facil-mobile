import 'package:denuncia_facil_mobile/app/models/ocurrence_model.dart';
import 'package:denuncia_facil_mobile/app/shared/styles.dart';
import 'package:denuncia_facil_mobile/app/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailOcurrenceView extends GetView {
  final OcurrenceModel ocurrence;

  DetailOcurrenceView(this.ocurrence);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        title: "Detalhe de ocorrência",
      ),
      body: Stack(
        children: [
          Positioned(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Image.network(
                    ocurrence.image,
                    height: MediaQuery.of(context).size.height * 0.35,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(12),
                  child: Text(
                    ocurrence.occurrenceType,
                    style: textprimary25,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(12),
                  child: Text(
                    ocurrence.occurrenceTitle,
                    style: textGrey18,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(12),
                  child: Text(
                    ocurrence.observation,
                    style: textGrey15,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 25,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Text(
                              "Status: ",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: textGrey18,
                            ),
                          ),
                          Container(
                            child: Text(
                              "Enviado",
                              style: textprimary18,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.all(15),
                        child: Row(
                          children: [
                            Container(
                              child: Text(
                                "Placa: ",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: textGrey18,
                              ),
                            ),
                            Container(
                              child: Text(
                                ocurrence.licensePlate,
                                style: textprimary18,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          "Data: ",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: textGrey18,
                        ),
                      ),
                      Container(
                        child: Text(
                          ocurrence.createdAt,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: textGrey18,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
