import 'package:get/get.dart';

import 'package:denuncia_facil_mobile/app/modules/historic_ocurrence/controllers/historic_ocurrence_controller.dart';

class HistoricOcurrenceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HistoricOcurrenceController>(
      () => HistoricOcurrenceController(),
    );
  }
}
