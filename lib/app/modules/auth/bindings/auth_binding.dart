import 'package:denuncia_facil_mobile/app/modules/auth/controllers/login_controller.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/controllers/register_controller.dart';
import 'package:denuncia_facil_mobile/app/modules/profile/controllers/profile_controller.dart';
import 'package:get/get.dart';

import 'package:denuncia_facil_mobile/app/modules/auth/controllers/auth_controller.dart';

class AuthBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AuthController>(
      () => AuthController(),
    );
    Get.lazyPut<LoginController>(
      () => LoginController(),
    );
    Get.lazyPut<RegisterController>(
      () => RegisterController(),
    );
    Get.lazyPut<ProfileController>(
      () => ProfileController(),
    );
  }
}
