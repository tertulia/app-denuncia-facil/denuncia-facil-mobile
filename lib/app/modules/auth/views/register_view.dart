import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/controllers/register_controller.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/views/login_view.dart';
import 'package:denuncia_facil_mobile/app/shared/buttons.dart';
import 'package:denuncia_facil_mobile/app/shared/inputs.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterView extends GetView<RegisterController> {
  @override
  Widget build(BuildContext context) {
    double sizeHeight = MediaQuery.of(context).size.height;
    ArsProgressDialog customProgressDialog = ArsProgressDialog(context,
        blur: 2,
        loadingWidget: Container(
          width: 50,
          height: 50,
          child: CircularProgressIndicator(),
        ));
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.all(20),
        children: [
          SizedBox(height: 40),
          Center(
            child: Image.asset(
              "assets/images/logo.png",
              height: sizeHeight * 0.12,
            ),
          ),
          SizedBox(height: 40),
          Container(
            alignment: Alignment.topLeft,
            child: Text("Registre-se",
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFFFD7A26))),
          ),
          Form(
            key: controller.formKeyLogin,
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20.0),
                  child: textFormField(
                    labText: "Nome",
                    validator: controller.validatorName,
                    textType: TextInputType.text,
                    cont: controller.controllerName,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20.0),
                  child: textFormField(
                    labText: "Telefone",
                    validator: controller.validatorPhone,
                    textType: TextInputType.phone,
                    cont: controller.controllerPhone,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20.0),
                  child: textFormField(
                    labText: "E-mail",
                    validator: controller.validatorEmail,
                    textType: TextInputType.emailAddress,
                    cont: controller.controllerEmail,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 15.0),
                  child: textFormField(
                    labText: "Senha",
                    validator: controller.validatorSenha,
                    obs: true,
                    cont: controller.controllerSenha,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  child: raisedButton(
                      onPress: () {
                        customProgressDialog.show();
                        controller.register(customProgressDialog);
                      },
                      text: "Cadastrar"),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text("Cadastre-se também por:"),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                        icon: Icon(
                          AntDesign.google,
                          size: 50,
                        ),
                        onPressed: () {}),
                    IconButton(
                        icon: Icon(
                          AntDesign.facebook_square,
                          size: 50,
                        ),
                        onPressed: () {}),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Já tem conta?"),
              flatButton(
                  text: "Login",
                  onPress: () {
                    Get.to(LoginView());
                  })
            ],
          ),
        ],
      ),
    );
  }
}
