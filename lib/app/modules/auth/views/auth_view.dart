import 'package:denuncia_facil_mobile/app/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/controllers/auth_controller.dart';

class AuthView extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    controller.onInit();
    return Scaffold(
      backgroundColor: primary,
      body: Center(
        child: Image.asset("assets/images/logo_branco.png"),
      ),
    );
  }
}
