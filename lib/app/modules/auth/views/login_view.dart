import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/controllers/login_controller.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/views/register_view.dart';
import 'package:denuncia_facil_mobile/app/shared/buttons.dart';
import 'package:denuncia_facil_mobile/app/shared/colors.dart';
import 'package:denuncia_facil_mobile/app/shared/inputs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    ArsProgressDialog customProgressDialog = ArsProgressDialog(context,
        blur: 2,
        loadingWidget: Container(
          width: 50,
          height: 50,
          child: CircularProgressIndicator(),
        ));
    double sizeHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(sizeHeight * 0.3),
        child: Container(
          decoration: BoxDecoration(
            color: primary,
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(20),
            ),
          ),
          child: Center(
            child: Image.asset(
              "assets/images/logo_branco.png",
              height: sizeHeight * 0.1,
            ),
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(15),
        child: Form(
          key: controller.formKeyLogin,
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 20.0),
                child: textFormField(
                  labText: "E-mail",
                  validator: controller.validatorEmail,
                  textType: TextInputType.emailAddress,
                  cont: controller.controllerEmail,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 15.0),
                child: textFormField(
                  labText: "Senha",
                  validator: controller.validatorSenha,
                  obs: true,
                  cont: controller.controllerSenha,
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                child: flatButton(
                    text: "Esqueceu a senha?",
                    onPress: controller.recuperSenha),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 12),
                child: raisedButton(
                    onPress: () async {
                      customProgressDialog.show();
                      controller.login(customProgressDialog);
                    },
                    text: "Entrar"),
              ),
              Container(
                alignment: Alignment.center,
                child: Text("Entre também por:"),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                      icon: Icon(
                        AntDesign.instagram,
                        size: 40,
                      ),
                      onPressed: () {}),
                  IconButton(
                      icon: Icon(
                        AntDesign.facebook_square,
                        size: 40,
                      ),
                      onPressed: () {}),
                ],
              ),
            ],
          ),
        ),
      ),
      bottomSheet: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Não tem conta?"),
          flatButton(
              text: "Registre-se",
              onPress: () {
                Get.to(RegisterView());
              })
        ],
      ),
    );
  }
}
