import 'dart:convert';

import 'package:denuncia_facil_mobile/app/data/user_data.dart';
import 'package:denuncia_facil_mobile/app/modules/home/views/home_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class RegisterController extends GetxController {
  String url =
      'https://api-denuncia-facil.herokuapp.com/api/accounts/register/';

  final controllerEmail = TextEditingController().obs();
  final controllerSenha = TextEditingController().obs();
  final controllerName = TextEditingController().obs();
  final controllerPhone = TextEditingController().obs();

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final formKeyLogin = GlobalKey<FormState>();

  String validatorEmail(textEmail) {
    if (textEmail.isEmpty) {
      return 'email obrigatorio';
    } else if (!textEmail.contains("@") || !textEmail.contains(".com")) {
      return 'email invalido';
    }
    return null;
  }

  String validatorSenha(textSenha) {
    if (textSenha.isEmpty) {
      return 'senha obrigatoria';
    } else if (textSenha.length < 4) {
      return 'no minimo 4 caracteres';
    }
    return null;
  }

  String validatorName(textName) {
    if (textName.isEmpty) {
      return 'nome obrigatorio';
    }
    return null;
  }

  String validatorPhone(textPhone) {
    if (textPhone.isEmpty) {
      return 'telefone obrigatorio';
    }
    return null;
  }

  void register(customProgressDialog) async {
    if (formKeyLogin.currentState.validate()) {
      final SharedPreferences prefs = await _prefs;

      try {
        var response = await http.post(url, body: {
          'email': controllerEmail.text,
          'password': controllerSenha.text,
          'name': controllerName.text,
          'cellphone': controllerPhone.text
        });
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');
        var userData = json.decode(response.body);

        await prefs.setString("email", userData["email"]);
        await prefs.setString("name", userData["name"]);
        await prefs.setString("phone", userData["cellphone"]);
        await prefs.setString("token", userData["token"]);

        nameUser = prefs.getString("name");
        emailUser = prefs.getString("email");
        phoneUser = prefs.getString("phone");
        token = prefs.getString("token");
        customProgressDialog.dismiss();
        Get.offAll(HomeView());
      } catch (e) {
        customProgressDialog.dismiss();
        print(e.toString());
      }
    }
    customProgressDialog.dismiss();
  }
}
