import 'package:denuncia_facil_mobile/app/data/user_data.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/views/login_view.dart';
import 'package:denuncia_facil_mobile/app/modules/home/views/home_view.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthController extends GetxController {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  void onInit() async {
    final SharedPreferences prefs = await _prefs;

    nameUser = prefs.getString("name");
    emailUser = prefs.getString("email");
    phoneUser = prefs.getString("phone");
    token = prefs.getString("token");
    token == null ? Get.off(LoginView()) : Get.off(HomeView());
  }
}
