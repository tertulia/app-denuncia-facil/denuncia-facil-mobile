import 'dart:convert';

import 'package:denuncia_facil_mobile/app/data/user_data.dart';
import 'package:denuncia_facil_mobile/app/modules/home/views/home_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginController extends GetxController {
  String url = 'https://api-denuncia-facil.herokuapp.com/api/accounts/login/';
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final controllerEmail = TextEditingController().obs();
  final controllerSenha = TextEditingController().obs();
  String tokenUser;

  final formKeyLogin = GlobalKey<FormState>();

  String validatorEmail(textEmail) {
    if (textEmail.isEmpty) {
      return 'email obrigatorio';
    } else if (!textEmail.contains("@") || !textEmail.contains(".com")) {
      return 'email invalido';
    }
    return null;
  }

  String validatorSenha(textSenha) {
    if (textSenha.isEmpty) {
      return 'senha obrigatoria';
    } else if (textSenha.length < 4) {
      return 'no minimo 4 caracteres';
    }
    return null;
  }

  void login(customProgressDialog) async {
    final SharedPreferences prefs = await _prefs;

    if (formKeyLogin.currentState.validate()) {
      try {
        var response = await http.post(
          url,
          body: {
            'email': controllerEmail.text,
            'password': controllerSenha.text
          },
        );
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');

        var userData = json.decode(response.body);

        await prefs.setString("email", userData["email"]);
        await prefs.setString("name", userData["name"]);
        await prefs.setString("phone", userData["cellphone"]);
        await prefs.setString("token", userData["token"]);

        nameUser = prefs.getString("name");
        emailUser = prefs.getString("email");
        phoneUser = prefs.getString("phone");
        token = prefs.getString("token");
        tokenUser = prefs.getString("token");
        customProgressDialog.dismiss();

        Get.offAll(HomeView());
      } catch (e) {
        print(e.toString());
        customProgressDialog.dismiss();
      }
    }
    customProgressDialog.dismiss();
  }

  recuperSenha() async {
    const url = 'https://api-denuncia-facil.herokuapp.com/reset_password/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
