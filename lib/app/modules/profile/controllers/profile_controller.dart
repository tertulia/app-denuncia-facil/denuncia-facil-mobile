import 'package:denuncia_facil_mobile/app/data/user_data.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class ProfileController extends GetxController {
  String url =
      'https://api-denuncia-facil.herokuapp.com/api/accounts/updateprofile/';

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final controllerEmail = TextEditingController().obs();
  final controllerName = TextEditingController().obs();
  final controllerPhone = TextEditingController().obs();

  final formKeyUpdate = GlobalKey<FormState>();

  String email;

  String validatorEmail(textEmail) {
    if (textEmail.isEmpty) {
      return 'email obrigatorio';
    } else if (!textEmail.contains("@") || !textEmail.contains(".com")) {
      return 'email invalido';
    }
    return null;
  }

  String validatorName(textName) {
    if (textName.isEmpty) {
      return 'nome obrigatorio';
    }
    return null;
  }

  String validatorPhone(textPhone) {
    if (textPhone.isEmpty) {
      return 'telefone obrigatorio';
    }
    return null;
  }

  void updateUser() async {
    final SharedPreferences prefs = await _prefs;

    if (formKeyUpdate.currentState.validate()) {
      try {
        var response = await http.put(
          url,
          body: {
            'name': controllerName.text,
            'cellphone': controllerPhone.text
          },
          headers: {'Authorization': 'Token $token'},
        );
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');

        var userData = json.decode(response.body);
        prefs.setString("name", userData["name"]);
        prefs.setString("phone", userData["cellphone"]);

        nameUser = prefs.getString("name");
        phoneUser = prefs.getString("phone");

        //Get.offAll(ProfileView());
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
