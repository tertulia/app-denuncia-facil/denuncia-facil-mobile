import 'package:denuncia_facil_mobile/app/data/user_data.dart';
import 'package:denuncia_facil_mobile/app/drawer/drawer.dart';
import 'package:denuncia_facil_mobile/app/shared/buttons.dart';
import 'package:denuncia_facil_mobile/app/shared/inputs.dart';
import 'package:denuncia_facil_mobile/app/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:denuncia_facil_mobile/app/modules/profile/controllers/profile_controller.dart';

class ProfileView extends GetView<ProfileController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        title: "Perfil",
      ),
      drawer: drawerWidget(context),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: [
          SizedBox(height: 20),
          Center(
              child: Container(
            height: 140,
            width: 140,
            decoration: BoxDecoration(
              color: Colors.grey[300],
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(30),
              image: new DecorationImage(
                  image: NetworkImage(
                      "https://t2.ea.ltmcdn.com/pt/images/6/0/8/img_alimentacao_do_coala_20806_orig.jpg"),
                  fit: BoxFit.fill),
            ),
          )),
          SizedBox(height: 20),
          Form(
            key: controller.formKeyUpdate,
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10.0),
                  child: textFormField(
                    setText: nameUser,
                    labText: "Nome",
                    validator: controller.validatorName,
                    textType: TextInputType.text,
                    cont: controller.controllerName,
                  ),
                ),
                Container(
                    margin: const EdgeInsets.symmetric(vertical: 10.0),
                    child: textFormField(
                      enab: false,
                      setText: emailUser,
                      labText: "E-mail",
                      validator: controller.validatorEmail,
                      textType: TextInputType.emailAddress,
                      cont: controller.controllerEmail,
                    )),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10.0),
                  child: textFormField(
                    setText: phoneUser,
                    labText: "Telefone",
                    validator: controller.validatorPhone,
                    textType: TextInputType.phone,
                    cont: controller.controllerPhone,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 40),
                  child: raisedButton(
                      onPress: controller.updateUser, text: "Atualizar"),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
