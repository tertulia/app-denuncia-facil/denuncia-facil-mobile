import 'package:denuncia_facil_mobile/app/drawer/drawer.dart';
import 'package:denuncia_facil_mobile/app/routes/app_pages.dart';
import 'package:denuncia_facil_mobile/app/shared/styles.dart';
import 'package:denuncia_facil_mobile/app/shared/widgets/app_bar.dart';
import 'package:denuncia_facil_mobile/app/shared/widgets/card_home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:denuncia_facil_mobile/app/modules/home/controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        title: "Home",
      ),
      drawer: drawerWidget(context),
      body: ListView(
        children: [
          CardHomeWidget(
              image: "assets/images/rampa.jpg",
              text: "Um jabuti na traseira da bike de um pneu so"),
          CardHomeWidget(
              image: "assets/images/canetinha.jpg",
              text: "Canetada por andar devagar demais"),
          CardHomeWidget(
              image: "assets/images/corno_estacionando.jpg",
              text: "Rua limpa demais"),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Get.toNamed(Routes.REGISTER_OCCURRENCE);
        },
        label: Text(
          "Registrar ocorrência",
          style: titleBar,
        ),
        isExtended: true,
      ),
    );
  }
}
