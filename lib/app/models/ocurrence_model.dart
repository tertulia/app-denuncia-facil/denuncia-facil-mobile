class OcurrenceModel {
  int id;
  String licensePlate;
  String occurrenceType;
  String image;
  var anonymous;
  String occurrenceTitle;
  String observation;
  String location;
  String createdAt;

  OcurrenceModel(
      {this.id,
      this.licensePlate,
      this.occurrenceType,
      this.image,
      this.anonymous,
      this.occurrenceTitle,
      this.observation,
      this.location,
      this.createdAt});

  OcurrenceModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    licensePlate = json['license_plate'];
    occurrenceType = json['occurrence_type'];
    image = json['image'];
    anonymous = json['anonymous'];
    occurrenceTitle = json['occurrence_title'];
    observation = json['observation'];
    location = json['location'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['license_plate'] = this.licensePlate;
    data['occurrence_type'] = this.occurrenceType;
    data['occurrence_title'] = this.occurrenceTitle;
    data['image'] = this.image;
    data['anonymous'] = this.anonymous;
    data['observation'] = this.observation;
    data['location'] = this.location;
    return data;
  }
}
