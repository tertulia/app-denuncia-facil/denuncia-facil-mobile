part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  static const HOME = '/home';
  static const AUTH = '/auth';
  static const PROFILE = '/profile';
  static const REGISTER_OCCURRENCE = '/register-occurrence';
  static const HISTORIC_OCURRENCE = '/historic-ocurrence';
}