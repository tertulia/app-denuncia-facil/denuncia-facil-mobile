import 'package:denuncia_facil_mobile/app/modules/historic_ocurrence/views/historic_ocurrence_view.dart';
import 'package:denuncia_facil_mobile/app/modules/historic_ocurrence/bindings/historic_ocurrence_binding.dart';
import 'package:denuncia_facil_mobile/app/modules/register_occurrence/views/register_occurrence_controller.dart';
import 'package:denuncia_facil_mobile/app/modules/register_occurrence/bindings/register_occurrence_binding.dart';
import 'package:denuncia_facil_mobile/app/modules/profile/views/profile_view.dart';
import 'package:denuncia_facil_mobile/app/modules/profile/bindings/profile_binding.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/views/auth_view.dart';
import 'package:denuncia_facil_mobile/app/modules/auth/bindings/auth_binding.dart';
import 'package:denuncia_facil_mobile/app/modules/home/views/home_view.dart';
import 'package:denuncia_facil_mobile/app/modules/home/bindings/home_binding.dart';
import 'package:get/get.dart';
part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.AUTH;

  static final routes = [
    GetPage(
      name: Routes.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.AUTH,
      page: () => AuthView(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.PROFILE,
      page: () => ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: Routes.REGISTER_OCCURRENCE,
      page: () => RegisterOccurrenceView(),
      binding: RegisterOccurrenceBinding(),
    ),
    GetPage(
      name: Routes.HISTORIC_OCURRENCE,
      page: () => HistoricOcurrenceView(),
      binding: HistoricOcurrenceBinding(),
    ),
  ];
}
