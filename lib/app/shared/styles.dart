import 'package:denuncia_facil_mobile/app/shared/colors.dart';
import 'package:flutter/material.dart';

TextStyle textGrey11 = TextStyle(
  color: grey,
  fontSize: 11,
);
TextStyle textprimary11 = TextStyle(
  color: primary,
  fontSize: 11,
);
TextStyle textGrey14 = TextStyle(
  color: greyDark,
  fontSize: 14,
);
TextStyle textprimary14 = TextStyle(
  color: primary,
  fontSize: 14,
);

TextStyle textGrey15 = TextStyle(
  color: greyDark,
  fontSize: 15,
);
TextStyle textprimary15 = TextStyle(
  color: primary,
  fontSize: 15,
);
TextStyle textGrey18 = TextStyle(
  color: greyDark,
  fontSize: 18,
  fontWeight: FontWeight.w600,
);
TextStyle textprimary18 = TextStyle(
  color: primary,
  fontSize: 18,
  fontWeight: FontWeight.w600,
);
TextStyle titleBar = TextStyle(
  color: white,
  fontSize: 18,
);
TextStyle textprimary25 = TextStyle(
  color: primary,
  fontSize: 25,
  fontWeight: FontWeight.w600,
);
TextStyle textprimary28 = TextStyle(
  color: primary,
  fontSize: 28,
  fontWeight: FontWeight.w600,
);
TextStyle textGrey28 = TextStyle(
  color: grey,
  fontSize: 28,
  fontWeight: FontWeight.w600,
);
