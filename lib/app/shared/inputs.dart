import 'package:denuncia_facil_mobile/app/shared/colors.dart';
import 'package:flutter/material.dart';

Container textFormField({
  String setText,
  cont,
  bool enab = true,
  bool obs = false,
  Icon icon,
  String initValue,
  int lines = 1,
  Function validator,
  TextInputType textType = TextInputType.text,
  String labText,
  // Widget suf,
}) {
  if (cont.text == "" || cont.text == null) {
    cont.text = setText;
  }

  return Container(
    margin: EdgeInsets.symmetric(horizontal: 30),
    child: TextFormField(
      controller: cont,
      validator: validator,
      enabled: enab,
      cursorColor: primary,
      initialValue: initValue,
      obscureText: obs,
      keyboardType: textType,
      maxLines: lines,
      decoration: InputDecoration(
        // suffix: suf,
        suffixIcon: icon,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
          gapPadding: 10,
        ),
        labelText: labText,

        // hintText: hint,
      ),
    ),
  );
}
