import 'package:flutter/material.dart';

import '../styles.dart';

AppBar appBar({String title}) {
  return AppBar(
    iconTheme: IconThemeData(
      color: Colors.white, //modify arrow color from here..
    ),
    title: Text(title, style: titleBar),
    centerTitle: true,
  );
}
