import 'package:denuncia_facil_mobile/app/shared/colors.dart';
import 'package:denuncia_facil_mobile/app/shared/styles.dart';
import 'package:flutter/material.dart';

class CardHomeWidget extends StatelessWidget {
  final String image;
  final String text;

  const CardHomeWidget({Key key, this.image, this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      shadowColor: primary,
      child: Container(
        height: 200,
        child: Stack(
          children: [
            Positioned(
              left: 0,
              child: Container(
                child: Image.asset(
                  image,
                  height: 200,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              right: 0,
              child: Container(
                padding: EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width * 0.5,
                height: 200,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Casos de denuncia",
                        style: textprimary18,
                      ),
                    ),
                    Container(
                      child: Text(
                        text,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: textGrey14,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Referente ao artigo: 271/20",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: textGrey11,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      elevation: 2,
    );
  }
}
