import 'package:denuncia_facil_mobile/app/models/ocurrence_model.dart';
import 'package:denuncia_facil_mobile/app/modules/historic_ocurrence/views/detail_ocurrence_view.dart';
import 'package:denuncia_facil_mobile/app/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CardHistoricWidget extends StatelessWidget {
  final OcurrenceModel ocurrence;

  const CardHistoricWidget({Key key, this.ocurrence}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Get.to(DetailOcurrenceView(ocurrence)),
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          height: 180,
          padding: EdgeInsets.all(8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "0001",
                      style: textprimary15,
                    ),
                  ),
                  Container(
                    child: Text(
                      "Placa: OJP-3333",
                      style: textprimary15,
                    ),
                  ),
                ],
              ),
              Container(
                child: Text(
                  ocurrence.occurrenceTitle,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: textGrey14,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Container(
                        child: Text(
                          "Status: ",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: textGrey14,
                        ),
                      ),
                      Container(
                        child: Text(
                          "Enviado",
                          style: textprimary15,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          "Data: ",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: textGrey14,
                        ),
                      ),
                      Container(
                        child: Text(
                          "21/12/2020",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: textGrey14,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        elevation: 2,
      ),
    );
  }
}
