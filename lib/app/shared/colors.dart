import 'package:flutter/material.dart';

Color primary = Color(0xFFFD7A26);
Color back = Color(0xFFF6F6F6);
Color white = Colors.white;
Color grey = Colors.grey;
Color greyDark = Colors.grey[600];
