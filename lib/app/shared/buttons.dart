import 'package:denuncia_facil_mobile/app/shared/colors.dart';
import 'package:flutter/material.dart';

Container flatButton({
  Function onPress,
  @required String text,
}) {
  return Container(
    child: FlatButton(
      onPressed: onPress,
      child: Text(
        text,
        style: TextStyle(
          color: primary,
        ),
      ),
    ),
  );
}

Container raisedButton({
  Function onPress,
  @required String text,
}) {
  return Container(
    child: Align(
      child: RaisedButton(
        padding: EdgeInsets.symmetric(
          horizontal: 40,
          vertical: 15,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50.0),
        ),
        color: Colors.amber[900],
        textColor: Colors.white,
        onPressed: onPress,
        child: Text(
          text,
          style: TextStyle(fontSize: 22),
        ),
      ),
    ),
  );
}

Container outLineButton({
  Function onPress,
  @required String text,
  @required Icon icon,
}) {
  return Container(
    padding: EdgeInsets.symmetric(
      horizontal: 25,
    ),
    child: OutlineButton(
      padding: EdgeInsets.all(20),
      shape: new RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      borderSide: BorderSide(color: Colors.grey),
      onPressed: onPress,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              text,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Colors.grey[600],
              ),
            ),
          ),
          icon,
        ],
      ),
    ),
  );
}
