import 'package:denuncia_facil_mobile/app/modules/auth/views/login_view.dart';
import 'package:denuncia_facil_mobile/app/routes/app_pages.dart';
import 'package:denuncia_facil_mobile/app/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

Drawer drawerWidget(BuildContext context) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 40),
          child: Container(
              height: 20, child: Image.asset("assets/images/logo_branco.png")),
          decoration: BoxDecoration(
            color: primary,
          ),
        ),
        ListTile(
          title: Text('Home'),
          onTap: () {
            Get.toNamed(Routes.HOME);
          },
        ),
        ListTile(
          title: Text('Historico'),
          onTap: () {
            Get.toNamed(Routes.HISTORIC_OCURRENCE);
          },
        ),
        ListTile(
          title: Text('Perfil'),
          onTap: () {
            Get.toNamed(Routes.PROFILE);
          },
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height * 0.40),
          child: ListTile(
            title: Text('Sair'),
            onTap: () async {
              SharedPreferences _prefs = await SharedPreferences.getInstance();
              _prefs.clear();
              Get.offAll(LoginView());
            },
          ),
        )
      ],
    ),
  );
}
